describe("storage provider", function() {
  'use strict';

  beforeEach(function() {
    module('appointlet.storage');
  });

  it("should be defined", inject(function(Storage) {
    expect(Storage).toBeDefined();
  }));
});
