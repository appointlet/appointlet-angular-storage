(function() {
  'use strict';

  angular.module('appointlet.storage', ['ipCookie'])

  /**
   * @ngdoc constant
   * @name storage.constant.StoragePrefix
   * @module appointlet.storage
   *
   * @description
   * Key prefix to use for storage backend.
   */
  .constant('StoragePrefix', 'appointlet')

  /**
   * @ngdoc service
   * @name storage.service.Storage
   * @module appointlet.storage
   *
   * @description
   *
   * Light wrapper around a storage mechanism for persisting and fetching
   * blobs of data.
   */
  .provider('Storage', function(StoragePrefix) {
    var domain;

    /**
     * @ngdoc function
     * @name storage.service.Storage#setDomain
     * @description sets the cookie domain
     */
    this.setDomain = function(d) {
      domain = d;
    };

    this.$get = function(ipCookie) {
      return {
        /**
         * @ngdoc property
         * @name storage.service.Storage#data
         * @description Data currently being stored.
         */
        data: ipCookie(StoragePrefix) || {},

        /**
         * @ngdoc function
         * @name storage.service.Storage#get
         * @description Retrieves a piece of data from persistent storage.
         * @param  {string} key to retrieve data for in the storage.
         */
        get: function(key) {
          return this.data[key];
        },

        /**
         * @ngdoc function
         * @name storage.service.Storage#create
         * @description Sets a piece of data in persistent storage.
         * @param  {string} key where to set the value
         * @param  {string} val data to store
         */
        set: function(key, val) {
          this.data[key] = val;
          ipCookie(StoragePrefix, this.data, {
            domain: domain,
            path: '/',
          });
        }
      };
    };
  });

})();
